O aplicativo criado tem por função trabalhar a capacidade visoespacial dos seus usuários,
ou seja, a habilidade de formular uma imagem e modifica-lá. Ao analisarmos como esse processo 
ocorre podemos descrever os seguintes passos:
	- O usuário clica em começar e é redirecionado;
	- O usuário recebe uma forma aletória dentro de 4 (quatro) opções;
	- Ao ser remanejado ele irá receber um comando dentro de 4 (quatro) possibilidades;
	- Finalmente ele é convidado a trabalhar a sua habilidade verificando,
        entre 4 (quatro) formas resultantes, qual é a corresponde a sua forma original após
	ser alterada pelos comandos de movimentação;
Cabe lembrar que o software foi desenvolvido utilizando o Solar 2D, com a biblioteca Composer 
(para manipulações de telas), e o Visual Studio Code como IDE para implementação do app.