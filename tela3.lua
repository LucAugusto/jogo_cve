local composer = require("composer");

local scene = composer.newScene();

c_x = display.contentWidth;
c_y = display.contentHeight;

function scene:create( event )
    
    local certo = audio.loadSound("Audios/certo.mp3");
    local errado = audio.loadSound("Audios/errado.mp3");
    local sceneGroup = self.view
    local params = event.params;

    local fundo = display.newImage("Imagens/fundo_tecido.jpg");
    fundo.x = display.contentCenterX;
    fundo.y = display.contentCenterY;
    sceneGroup:insert(fundo);

    local txtFig = display.newText("Escolha a figura correta",display.contentCenterX,c_y/2-200,native.systemFontBold,25);
    sceneGroup:insert(txtFig);

    local txtOrdem1 = display.newText("",display.contentCenterX,c_y/2-140,native.systemFont,20);
    sceneGroup:insert(txtOrdem1);
    local txtOrdem2 = display.newText("",display.contentCenterX,c_y/2-110,native.systemFont,20);
    sceneGroup:insert(txtOrdem2);

    local numAle = math.random(1,4);
    
        if numAle == 1 then
            txtOrdem1.text = "Vire uma vez pra direita!"
            txtOrdem2.text = "Vira uma vez pra esquerda!"
        end
        
        if numAle == 2 then
            txtOrdem1.text = "Vire uma vez pra esquerda!"
            txtOrdem2.text = "Vira uma vez pra esquerda!"
        end

        if numAle == 3 then
            txtOrdem1.text = "Vire uma vez pra esquerda!"
            txtOrdem2.text = "Vira duas vezes pra direita!"
        end

        if numAle == 4 then
            txtOrdem1.text = "Vire uma vez pra direita!"
            txtOrdem2.text = "Vira duas vezes pra esquerda!"
        end

    if params.qual == 1 then

        local tri1 = display.newImage("Imagens/triangulo.png");
        tri1.x = display.contentWidth - 70;
        tri1.y = display.contentCenterY;
        tri1:scale(0.6,0.6);
        sceneGroup:insert(tri1);

        local tri2 = display.newImage("Imagens/triangulo_180.png");
        tri2.x = display.contentWidth - 240;
        tri2.y = display.contentCenterY;
        tri2:scale(0.8,0.8);
        sceneGroup:insert(tri2);

        local tri3 = display.newImage("Imagens/triangulo_90_dir.png");
        tri3.x = display.contentWidth - 240;
        tri3.y = display.contentCenterY + 130;
        tri3:scale(0.8,0.8);
        sceneGroup:insert(tri3);

        local tri4 = display.newImage("Imagens/triangulo_90_esq.png");
        tri4.x = display.contentWidth - 70;
        tri4.y = display.contentCenterY + 130;
        tri4:scale(0.8,0.8);
        sceneGroup:insert(tri4);

        

        

        function triangulo(event)
              if numAle == 1 then
                  audio.play(certo);
                  
              else
                    audio.play(errado);
              end
        end

        function triangulo_180(event)
            if numAle == 2 then
                audio.play(certo);
            else
                  audio.play(errado);
            end
      end

      function triangulo_90_dir(event)
        if numAle == 3 then
            audio.play(certo);
        else
              audio.play(errado);
        end
  end

  function triangulo_90_esq(event)
    if numAle == 4 then
        audio.play(certo);
    else
          audio.play(errado);
    end
end


        tri1:addEventListener("tap", triangulo);
        tri2:addEventListener("tap", triangulo_180);
        tri3:addEventListener("tap", triangulo_90_dir);
        tri4:addEventListener("tap", triangulo_90_esq);
    end

    if params.qual == 2 then
        local ret1 = display.newImage("Imagens/retangulo.png");
        ret1.x = display.contentWidth - 70;
        ret1.y = display.contentCenterY;
        ret1:scale(0.8,0.8);
        sceneGroup:insert(ret1);

        local ret2 = display.newImage("Imagens/retangulo_180.png");
        ret2.x = display.contentWidth - 240;
        ret2.y = display.contentCenterY;
        ret2:scale(0.8,0.8);
        sceneGroup:insert(ret2);

        local ret3 = display.newImage("Imagens/retangulo_90_dir.png");
        ret3.x = display.contentWidth - 240;
        ret3.y = display.contentCenterY + 130;
        ret3:scale(0.8,0.8);
        sceneGroup:insert(ret3);

        local ret4 = display.newImage("Imagens/retangulo_90_esq.png");
        ret4.x = display.contentWidth - 70;
        ret4.y = display.contentCenterY + 130;
        ret4:scale(0.8,0.8);
        sceneGroup:insert(ret4);

        function retangulo(event)
            if numAle == 1 then
                audio.play(certo);
                
            else
                  audio.play(errado);
            end
      end

      function retangulo_180(event)
          if numAle == 2 then
              audio.play(certo);
          else
                audio.play(errado);
          end
    end

    function retangulo_90_dir(event)
      if numAle == 3 then
          audio.play(certo);
      else
            audio.play(errado);
      end
    end

    function retangulo_90_esq(event)
    if numAle == 4 then
      audio.play(certo);
    else
        audio.play(errado);
    end
    end


      ret1:addEventListener("tap", retangulo);
      ret2:addEventListener("tap", retangulo_180);
      ret3:addEventListener("tap", retangulo_90_dir);
      ret4:addEventListener("tap", retangulo_90_esq);
        
    end

    if params.qual == 3 then
        local retp1 = display.newImage("Imagens/triangulo_pit.png");
        retp1.x = display.contentWidth - 70;
        retp1.y = display.contentCenterY;
        retp1:scale(0.8,0.8);
        sceneGroup:insert(retp1);

        local retp2 = display.newImage("Imagens/triangulo_pit_180.png");
        retp2.x = display.contentWidth - 240;
        retp2.y = display.contentCenterY;
        retp2:scale(0.8,0.8);
        sceneGroup:insert(retp2);

        local retp3 = display.newImage("Imagens/triangulo_pit_90_dir.png");
        retp3.x = display.contentWidth - 240;
        retp3.y = display.contentCenterY + 130;
        retp3:scale(0.8,0.8);
        sceneGroup:insert(retp3);

        local retp4 = display.newImage("Imagens/triangulo_pit_90_esq.png");
        retp4.x = display.contentWidth - 70;
        retp4.y = display.contentCenterY + 130;
        retp4:scale(0.8,0.8);
        sceneGroup:insert(retp4);

        function triangulo_pit(event)
            if numAle == 1 then
                audio.play(certo);
                
            else
                  audio.play(errado);
            end
      end

      function triangulo_pit_180(event)
          if numAle == 2 then
              audio.play(certo);
          else
                audio.play(errado);
          end
    end

    function triangulo_pit_90_dir(event)
      if numAle == 3 then
          audio.play(certo);
      else
            audio.play(errado);
      end
    end

    function triangulo_pit_90_esq(event)
    if numAle == 4 then
      audio.play(certo);
    else
        audio.play(errado);
    end
    end


      retp1:addEventListener("tap", triangulo_pit);
      retp2:addEventListener("tap", triangulo_pit_180);
      retp3:addEventListener("tap", triangulo_pit_90_dir);
      retp4:addEventListener("tap", triangulo_pit_90_esq);
        


    end

    if params.qual == 4 then
        local pent1 = display.newImage("Imagens/pentagono.png");
        pent1.x = display.contentWidth - 70;
        pent1.y = display.contentCenterY;
        pent1:scale(0.8,0.8);
        sceneGroup:insert(pent1);

        local pent2 = display.newImage("Imagens/pentagono_180.png");
        pent2.x = display.contentWidth - 240;
        pent2.y = display.contentCenterY;
        pent2:scale(0.8,0.8);
        sceneGroup:insert(pent2);

        local pent3 = display.newImage("Imagens/pentagono_90_dir.png");
        pent3.x = display.contentWidth - 240;
        pent3.y = display.contentCenterY + 130;
        pent3:scale(0.8,0.8);
        sceneGroup:insert(pent3);

        local pent4 = display.newImage("Imagens/pentagono_90_esq.png");
        pent4.x = display.contentWidth - 70;
        pent4.y = display.contentCenterY + 130;
        pent4:scale(0.8,0.8);
        sceneGroup:insert(pent4);

        function pentagono(event)
            if numAle == 1 then
                audio.play(certo);
                
            else
                  audio.play(errado);
            end
      end

      function pentagono_180(event)
          if numAle == 2 then
              audio.play(certo);
          else
                audio.play(errado);
          end
    end

    function pentagono_90_dir(event)
      if numAle == 3 then
          audio.play(certo);
      else
            audio.play(errado);
      end
    end

    function pentagono_90_esq(event)
    if numAle == 4 then
      audio.play(certo);
    else
        audio.play(errado);
    end
    end


      pent1:addEventListener("tap", pentagono);
      pent2:addEventListener("tap", pentagono_180);
      pent3:addEventListener("tap", pentagono_90_dir);
      pent4:addEventListener("tap", pentagono_90_esq);
        
        
    end


    local btnPlay = display.newImage("Imagens/botao_voltar.png");
    btnPlay:scale(0.3,0.3);
    btnPlay.x = display.contentCenterX;
    btnPlay.y = display.contentHeight-10;
    sceneGroup:insert(btnPlay);

    function voltarIni(event)

        opts = {
            effect = "slideLeft",
            time = 1000,
        }
        composer.removeHidden();
        composer.gotoScene("tela1",opts);
    end


    btnPlay:addEventListener("tap", voltarIni)


end

-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    
 
end
-- hide()

function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        
        
    end

end

-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        
 
    end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "hide", scene)
scene:addEventListener("destroy", scene)
scene:addEventListener("show",scene)
return scene