local composer = require("composer");

local scene = composer.newScene();

c_x = display.contentWidth;
c_y = display.contentHeight;

function scene:create( event )
    
    local sceneGroup = self.view

    local fundo = display.newImage("Imagens/fundo_tecido.jpg");
    fundo.x = display.contentCenterX;
    fundo.y = display.contentCenterY;
    sceneGroup:insert(fundo);

    local numAle = math.random(1,4);
    print(numAle);
    
    if numAle == 1 then
        local triangulo = display.newImage("Imagens/triangulo.png");
        triangulo.x = display.contentCenterX;
        triangulo.y = display.contentCenterY;
        sceneGroup:insert(triangulo);
    end

    if numAle == 2 then
        local retangulo = display.newImage("Imagens/retangulo.png");
        retangulo.x = display.contentCenterX-10;
        retangulo.y = display.contentCenterY;
        retangulo:scale(1.6,1.6);
        sceneGroup:insert(retangulo);
    end

    if numAle == 3 then
        local triangulo_pit = display.newImage("Imagens/triangulo_pit.png");
        triangulo_pit.x = display.contentCenterX;
        triangulo_pit.y = display.contentCenterY;
        triangulo_pit:scale(1.6,1.6);
        sceneGroup:insert(triangulo_pit);
        
    end

    if numAle == 4 then
        local pentagono = display.newImage("Imagens/pentagono.png");
        pentagono.x = display.contentCenterX;
        pentagono.y = display.contentCenterY;
        pentagono:scale(1.6,1.6);
        sceneGroup:insert(pentagono);
        
    end
   
    local txtFig = display.newText("Essa é sua figura!",display.contentCenterX,c_y/2-180,native.systemFontBold,30);
    sceneGroup:insert(txtFig);

    local txtRem = display.newText("Lembre-se dela!",display.contentCenterX,c_y/2+140,native.systemFont,20);
    sceneGroup:insert(txtRem);

    --local txtIni = display.newText("Pronto?",display.contentCenterX,c_y/2+180,native.systemFontBold,25);
    --sceneGroup:insert(txtIni);

    local btnPlay = display.newImage("Imagens/botao.png");
    btnPlay:scale(0.35,0.35);
    btnPlay.x = display.contentCenterX;
    btnPlay.y = display.contentHeight-10;
    sceneGroup:insert(btnPlay);
    
    function comecar(event)
          local opts = {
              effect = "slideRight",
              time = 1000,
              params = {
                  qual = numAle;
              }
          }
          composer.removeHidden();
          composer.gotoScene("tela3",opts);
    end


    btnPlay:addEventListener("tap", comecar );

end

-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    
 
end
-- hide()

function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        
        
    end

end

-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        
 
    end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "hide", scene)
scene:addEventListener("destroy", scene)
scene:addEventListener("show",scene)
return scene