local composer = require("composer");

local scene = composer.newScene();

c_x = display.contentWidth;
c_y = display.contentHeight;

function scene:create( event )
    
    local sceneGroup = self.view

    local fundo = display.newImage("Imagens/fundo_tecido.jpg");
    fundo.x = display.contentCenterX;
    fundo.y = display.contentCenterY;
    sceneGroup:insert(fundo);

    local txtIni = display.newText("Pronto?",display.contentCenterX,c_y/2-30,native.systemFontBold,45);
    sceneGroup:insert(txtIni);

    local btnPlay = display.newImage("Imagens/botao.png");
    btnPlay:scale(0.35,0.35);
    btnPlay.x = display.contentCenterX;
    btnPlay.y = display.contentCenterY+110;
    sceneGroup:insert(btnPlay);

    local txtMedium = display.newText("Começar",c_x/2,c_y/2+50,native.systemFontBold,20);
    sceneGroup:insert(txtMedium);
   
    
    local ret = display.newImage("Imagens/ret_sem_tri.png");
    ret:scale(1.5,0.15);
    ret.x = display.contentCenterX-10;
    ret.y = display.contentCenterY+10;
    sceneGroup:insert(ret);
  
    function play(event)
        local opts = {
            effect = slideRight,
            time = 1000,
        }
        composer.removeHidden();
        composer.gotoScene("tela2",opts);
    end

    btnPlay:addEventListener("tap", play )
end

-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    
 
end
-- hide()

function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        
        
    end

end

-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        
 
    end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "hide", scene)
scene:addEventListener("destroy", scene)
scene:addEventListener("show",scene)
return scene